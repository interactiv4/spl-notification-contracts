<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

/**
 * Interface NotificationMessageInterface.
 *
 * Representation of an outgoing, client-side notification request.
 *
 * The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY",
 * and "OPTIONAL" in this class docs are to be interpreted as described in RFC 2119.
 *
 * @see https://microformats.org/wiki/rfc-2119#Definitions
 *
 * During construction, implementations MUST attempt to set the title, body, notification level and additional data,
 * being notification level one of NotificationLevelInterface::LEVELS_MAP values.
 *
 * Notifications messages are considered immutable; all methods that might change state MUST be implemented such that
 * they retain the internal state of the current message and return an instance that contains the changed state.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
interface NotificationMessageInterface
{
    /**
     * Get notification message title.
     *
     * This method SHOULD NOT throw any exception but RuntimeException.
     *
     * @return string|null Avoid returning an empty string; return null instead.
     */
    public function getTitle(): ?string;

    /**
     * Get notification message body.
     *
     * This method SHOULD NOT throw any exception but RuntimeException.
     *
     * @return string|null Avoid returning an empty string; return null instead.
     */
    public function getBody(): ?string;

    /**
     * Get notification message level.
     *
     * This method SHOULD NOT throw any exception but RuntimeException.
     *
     * @return int One of NotificationLevelInterface::LEVELS_MAP values
     */
    public function getLevel(): int;

    /**
     * Get notification message mixed additional data. It fully depends on the implementation.
     * There are not any assumptions about returned content that can be made by implementors.
     *
     * @return array
     */
    public function getAdditionalData(): array;
}
