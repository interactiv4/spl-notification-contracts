<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

/**
 * Trait NotificationMessageTrait.
 *
 * Help trait to implement @see NotificationMessageInterface.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
trait NotificationMessageTrait
{
    /**
     * @var string|null
     */
    protected $notificationTitle;

    /**
     * @var string|null
     */
    protected $notificationBody;

    /**
     * @var int
     */
    protected $notificationLevel;

    /**
     * @var array
     */
    protected $notificationAdditionalData = [];

    /**
     * {@inheritdoc}
     */
    public function getTitle(): ?string
    {
        return '' !== (string)$this->notificationTitle
            ? $this->notificationTitle
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getBody(): ?string
    {
        return '' !== (string)$this->notificationBody
            ? $this->notificationBody
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getLevel(): int
    {
        return $this->notificationLevel;
    }

    /**
     * {@inheritdoc}
     */
    public function getAdditionalData(): array
    {
        return $this->notificationAdditionalData;
    }
}
