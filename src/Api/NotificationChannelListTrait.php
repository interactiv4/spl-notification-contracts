<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

/**
 * Trait NotificationChannelListTrait.
 *
 * Help trait to implement @see NotificationChannelListInterface.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
trait NotificationChannelListTrait
{
    /**
     * @var NotificationChannelInterface[]
     */
    protected $notificationChannels = [];

    /**
     * {@inheritdoc}
     */
    public function get(int $level): array
    {
        return \array_filter(
            $this->notificationChannels,
            static function ($notificationChannel) use ($level) {
                return $notificationChannel->supports($level);
            }
        );
    }

    /**
     * Add notification channel.
     * Protected utility method to add notification channels with strict type checking.
     * May be used in implementors constructor methods.
     *
     * @param NotificationChannelInterface $notificationChannel
     *
     * @return void
     */
    protected function addNotificationChannel(NotificationChannelInterface $notificationChannel): void
    {
        $this->notificationChannels[] = $notificationChannel;
    }
}
