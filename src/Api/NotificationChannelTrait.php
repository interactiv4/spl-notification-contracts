<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelMaskException;

/**
 * Trait NotificationChannelTrait.
 *
 * Help trait to implement @see NotificationChannelInterface.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
trait NotificationChannelTrait
{
    /**
     * @var NotificationLevelInterface
     */
    protected $notificationLevel;

    /**
     * @var int
     */
    protected $notificationLevelMask;

    /**
     * {@inheritdoc}
     */
    public function supports(int $level): bool
    {
        try {
            return $this->notificationLevel->satisfies($this->notificationLevelMask, $level);
        } catch (InvalidNotificationLevelMaskException $e) {
            return false;
        }
    }
}
