<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api\Exception;

/**
 * Class InvalidNotificationLevelException.
 *
 * Thrown when level does not match one of NotificationLevelInterface::LEVELS_MAP values.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
final class InvalidNotificationLevelException extends NotificationException
{
}
