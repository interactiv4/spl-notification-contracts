<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api\Exception;

/**
 * Class InvalidNotificationLevelMaskException.
 *
 * Thrown when level mask is not valid according to available levels.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
final class InvalidNotificationLevelMaskException extends NotificationException
{
}
