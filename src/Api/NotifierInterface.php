<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\CouldNotSendNotificationMessageException;

/**
 * Interface NotifierInterface.
 *
 * Main facade interface for sending notifications.
 *
 * For implementing this class, you may want to use:
 *
 * @see NotifierCompositePureTrait
 * @see NotifierCompositeChannelListTrait
 *
 * The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY",
 * and "OPTIONAL" in this class docs are to be interpreted as described in RFC 2119.
 *
 * @see https://microformats.org/wiki/rfc-2119#Definitions
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
interface NotifierInterface
{
    /**
     * Send a notification.
     *
     * @param int         $level One of NotificationLevelInterface::LEVELS_MAP values
     * @param string|null $title Notification title, if any
     * @param string|null $body Notification message, if any
     * @param array       $additionalData Mixed additional data. It fully depends on the implementation.
     *
     * @return void Returning without raising an exception is the way to communicate everything is ok.
     *
     * @throws CouldNotSendNotificationMessageException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function notify(
        int $level,
        ?string $title = null,
        ?string $body = null,
        array $additionalData = []
    ): void;
}
