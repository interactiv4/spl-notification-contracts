<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelException;

/**
 * Interface NotificationChannelListInterface.
 *
 * List of notification channels.
 *
 * @see NotificationChannelListTrait
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
interface NotificationChannelListInterface
{
    /**
     * Retrieve list of notification channels supporting specified level.
     *
     * @param int $level One of NotificationLevelInterface::LEVELS_MAP values
     *
     * @return NotificationChannelInterface[] An array of notification channels supporting specified level.
     *
     * @throws InvalidNotificationLevelException
     * - When level is not one of NotificationLevelInterface::LEVELS_MAP values
     */
    public function get(int $level): array;
}
