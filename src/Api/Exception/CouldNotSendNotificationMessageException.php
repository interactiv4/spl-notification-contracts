<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api\Exception;

/**
 * Class CouldNotSendNotificationException.
 *
 * Thrown when a non-runtime error occurs while sending a notification message.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
final class CouldNotSendNotificationMessageException extends NotificationException
{
}
