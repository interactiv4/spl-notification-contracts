<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api\Exception;

use Exception;

/**
 * Class NotificationException.
 *
 * All exceptions raised by notification interfaces MUST be either:
 * - Subclasses of this class
 * - @see \RuntimeException or its subclasses
 *
 * For @see \LogicException or more specific exceptions, wrap them as previous into appropriate exception type:
 *
 * @see BeginNotificationException
 * @see RollbackNotificationException
 * @see CommitNotificationException
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
abstract class NotificationException extends Exception
{
}
