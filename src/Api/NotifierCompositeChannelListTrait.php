<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\CouldNotSendNotificationMessageException;
use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelException;

/**
 * Trait NotifierCompositeChannelListTrait.
 *
 * Help trait to implement @see NotifierInterface as a composite.
 * This implementation uses a channel list to notify a message.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
trait NotifierCompositeChannelListTrait
{
    /**
     * @var NotificationChannelListInterface
     */
    protected $notificationChannelList;

    /**
     * @var NotificationMessageFactoryInterface
     */
    protected $notificationMessageFactory;

    /**
     * {@inheritdoc}
     */
    public function notify(
        int $level,
        ?string $title = null,
        ?string $body = null,
        array $additionalData = []
    ): void {
        $couldNotSendErrorMessages = [];

        try {
            $notificationMessage = $this->notificationMessageFactory->create($level, $title, $body, $additionalData);

            foreach ($this->notificationChannelList->get($level) as $notificationChannel) {
                try {
                    $notificationChannel->send($notificationMessage);
                } catch (CouldNotSendNotificationMessageException $e) {
                    $couldNotSendErrorMessages[] = $e->getMessage();
                }
            }
        } catch (InvalidNotificationLevelException $e) {
            throw new CouldNotSendNotificationMessageException(
                \sprintf(
                    'Could not send message: %s',
                    $e->getMessage()
                )
            );
        }

        if (\count($couldNotSendErrorMessages)) {
            throw new CouldNotSendNotificationMessageException(
                \sprintf(
                    'Some error(s) occurred while sending notifications: %' . PHP_EOL,
                    \implode(PHP_EOL, $couldNotSendErrorMessages)
                )
            );
        }
    }
}
