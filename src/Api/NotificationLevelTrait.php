<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelException;
use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelMaskException;

/**
 * Trait NotificationLevelTrait.
 *
 * Help trait to implement @see NotificationLevelInterface.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
trait NotificationLevelTrait
{
    /**
     * {@inheritdoc}
     */
    public function checkLevelMask(int $levelMask): void
    {
        $minLevelMask = self::LEVEL_DEBUG;
        $maxLevelMask = \array_sum(self::LEVELS_MAP);

        if ($levelMask < $minLevelMask || $levelMask > $maxLevelMask) {
            throw new InvalidNotificationLevelMaskException(
                \sprintf(
                    'Invalid level mask supplied: %s, min level mask: %s, max level mask: %s',
                    $levelMask,
                    $minLevelMask,
                    $maxLevelMask
                )
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function checkLevel(int $level): void
    {
        if (!\in_array($level, self::LEVELS_MAP, true)) {
            throw new InvalidNotificationLevelException(
                \sprintf(
                    'Invalid level supplied: %s, expected one of: %s',
                    $level,
                    \implode(', ', self::LEVELS_MAP)
                )
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function satisfies(int $levelMask, int $level): bool
    {
        $this->checkLevel($level);
        $this->checkLevelMask($levelMask);

        return (bool)($levelMask & $level);
    }
}
