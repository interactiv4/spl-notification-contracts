<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\CouldNotSendNotificationMessageException;
use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelException;
use RuntimeException;

/**
 * Interface NotificationChannelInterface.
 *
 * Handle outgoing requests for notification messages to an specific endpoint.
 *
 * @see NotificationChannelTrait
 *
 * The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY",
 * and "OPTIONAL" in this class docs are to be interpreted as described in RFC 2119.
 *
 * @see https://microformats.org/wiki/rfc-2119#Definitions
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
interface NotificationChannelInterface
{
    /**
     * Send notification message.
     *
     * Level check is outside of this method responsibility.
     * If called, it MUST send the supplied message, without considering if this channel supports it or not.
     *
     * @param NotificationMessageInterface $message
     *
     * @return void Returning without raising an exception is the way to communicate everything is ok.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Network unavailable.
     *
     * @throws CouldNotSendNotificationMessageException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function send(NotificationMessageInterface $message): void;

    /**
     * Can this notification channel be used for notifying messages of given level?
     *
     * @param int $level One of NotificationLevelInterface::LEVELS_MAP values
     *
     * @return bool
     *
     * @throws InvalidNotificationLevelException
     */
    public function supports(int $level): bool;
}
