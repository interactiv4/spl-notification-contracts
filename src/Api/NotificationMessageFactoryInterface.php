<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelException;

/**
 * Interface NotificationMessageFactoryInterface.
 *
 * Common standard for factories that create NotificationMessageInterface objects.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
interface NotificationMessageFactoryInterface
{
    /**
     * Create notification message.
     *
     * @param int         $level One of NotificationLevelInterface::LEVELS_MAP values
     * @param string|null $title Notification title, if any
     * @param string|null $body Notification message, if any
     * @param array       $additionalData Mixed additional data. It fully depends on the implementation.
     *
     * @return NotificationMessageInterface
     *
     * @throws InvalidNotificationLevelException
     * - When level is not one of NotificationLevelInterface::LEVELS_MAP values
     */
    public function create(
        int $level,
        ?string $title = null,
        ?string $body = null,
        array $additionalData = []
    ): NotificationMessageInterface;
}
