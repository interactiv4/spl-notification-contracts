<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelException;
use Interactiv4\Contracts\SPL\Notification\Api\Exception\InvalidNotificationLevelMaskException;

/**
 * Interface NotificationLevelInterface.
 *
 * Notification levels, defined to be used as bitmasks.
 *
 * @see NotificationLevelTrait
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
interface NotificationLevelInterface
{
    /**
     * System is unusable.
     */
    public const LEVEL_EMERGENCY = 128;

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
     */
    public const LEVEL_ALERT = 64;

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     */
    public const LEVEL_CRITICAL = 32;

    /**
     * Runtime errors that do not require immediate action but should typically be logged and monitored.
     */
    public const LEVEL_ERROR = 16;

    /**
     * Exceptional occurrences that are not errors.
     */
    public const LEVEL_WARNING = 8;

    /**
     * Normal but significant events.
     */
    public const LEVEL_NOTICE = 4;

    /**
     * Interesting events.
     */
    public const LEVEL_INFO = 2;

    /**
     * Detailed debug information.
     */
    public const LEVEL_DEBUG = 1;

    /**
     * All levels map.
     */
    public const LEVELS_MAP = [
        'emergency' => self::LEVEL_EMERGENCY,
        'alert'     => self::LEVEL_ALERT,
        'critical'  => self::LEVEL_CRITICAL,
        'error'     => self::LEVEL_ERROR,
        'warning'   => self::LEVEL_WARNING,
        'notice'    => self::LEVEL_NOTICE,
        'info'      => self::LEVEL_INFO,
        'debug'     => self::LEVEL_DEBUG,
    ];

    /**
     * Handy level masks to specify a level or lower (<= than specified level).
     * For example, LEVEL_NOTICE_OR_LOWER would match notice, info and debug levels.
     */
    public const LEVEL_MASK_INFO_OR_LOWER      = self::LEVEL_INFO | self::LEVEL_DEBUG;
    public const LEVEL_MASK_NOTICE_OR_LOWER    = self::LEVEL_NOTICE | self::LEVEL_MASK_INFO_OR_LOWER;
    public const LEVEL_MASK_WARNING_OR_LOWER   = self::LEVEL_WARNING | self::LEVEL_MASK_NOTICE_OR_LOWER;
    public const LEVEL_MASK_ERROR_OR_LOWER     = self::LEVEL_ERROR | self::LEVEL_MASK_WARNING_OR_LOWER;
    public const LEVEL_MASK_CRITICAL_OR_LOWER  = self::LEVEL_CRITICAL | self::LEVEL_MASK_ERROR_OR_LOWER;
    public const LEVEL_MASK_ALERT_OR_LOWER     = self::LEVEL_ALERT | self::LEVEL_MASK_CRITICAL_OR_LOWER;
    public const LEVEL_MASK_EMERGENCY_OR_LOWER = self::LEVEL_EMERGENCY | self::LEVEL_MASK_ALERT_OR_LOWER;

    /**
     * Handy level masks to specify a level or higher (>= than specified level).
     * For example, LEVEL_NOTICE_OR_HIGHER would match notice, info and debug levels.
     */
    public const LEVEL_MASK_ALERT_OR_HIGHER    = self::LEVEL_ALERT | self::LEVEL_EMERGENCY;
    public const LEVEL_MASK_CRITICAL_OR_HIGHER = self::LEVEL_CRITICAL | self::LEVEL_MASK_ALERT_OR_HIGHER;
    public const LEVEL_MASK_ERROR_OR_HIGHER    = self::LEVEL_ERROR | self::LEVEL_MASK_CRITICAL_OR_HIGHER;
    public const LEVEL_MASK_WARNING_OR_HIGHER  = self::LEVEL_WARNING | self::LEVEL_MASK_ERROR_OR_HIGHER;
    public const LEVEL_MASK_INFO_OR_HIGHER     = self::LEVEL_INFO | self::LEVEL_MASK_WARNING_OR_HIGHER;
    public const LEVEL_MASK_NOTICE_OR_HIGHER   = self::LEVEL_NOTICE | self::LEVEL_MASK_INFO_OR_HIGHER;
    public const LEVEL_MASK_DEBUG_OR_HIGHER    = self::LEVEL_DEBUG | self::LEVEL_MASK_NOTICE_OR_HIGHER;

    /**
     * Special mask alias to refer to all levels without referencing directly to higher level mask.
     */
    public const LEVEL_MASK_ALL = self::LEVEL_MASK_DEBUG_OR_HIGHER;

    /**
     * Check if supplied level mask is a valid level mask.
     *
     * @param int $levelMask
     *
     * @return void
     *
     * @throws InvalidNotificationLevelMaskException
     */
    public function checkLevelMask(int $levelMask): void;

    /**
     * Check if supplied level is amongst the available ones.
     *
     * @param int $level
     *
     * @return void
     *
     * @throws InvalidNotificationLevelException
     */
    public function checkLevel(int $level): void;

    /**
     * Check if supplied level mask satisfies specified level.
     *
     * @param int $levelMask
     * @param int $level
     *
     * @return bool
     *
     * @throws InvalidNotificationLevelException
     * @throws InvalidNotificationLevelMaskException
     */
    public function satisfies(
        int $levelMask,
        int $level
    ): bool;
}
