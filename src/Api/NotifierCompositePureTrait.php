<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\CouldNotSendNotificationMessageException;

/**
 * Trait NotifierCompositePureTrait.
 *
 * Help trait to implement @see NotifierInterface as a composite.
 * This implementation simply delegates to other notifiers.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Notification
 */
trait NotifierCompositePureTrait
{
    /**
     * @var NotifierInterface[]
     */
    protected $notifiers = [];

    /**
     * {@inheritdoc}
     */
    public function notify(
        int $level,
        ?string $title = null,
        ?string $body = null,
        array $additionalData = []
    ): void {
        $couldNotSendErrorMessages = [];

        foreach ($this->notifiers as $notifier) {
            try {
                $notifier->notify($level, $title, $body, $additionalData);
            } catch (CouldNotSendNotificationMessageException $e) {
                // Allow all notifiers to have a chance to send the message
                $couldNotSendErrorMessages[] = $e->getMessage();
            }
        }

        if (\count($couldNotSendErrorMessages)) {
            throw new CouldNotSendNotificationMessageException(
                \sprintf(
                    'Some error(s) occurred while sending notifications: %' . PHP_EOL,
                    \implode(PHP_EOL, $couldNotSendErrorMessages)
                )
            );
        }
    }

    /**
     * Add notifier.
     * Protected utility method to add notifiers with strict type checking.
     * May be used in implementors constructor methods.
     *
     * @param NotifierInterface $notifier
     *
     * @return void
     */
    protected function addNotifier(NotifierInterface $notifier): void
    {
        $this->notifiers[] = $notifier;
    }
}
